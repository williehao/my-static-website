const { Connection, Request } = require('tedious')
const dotenv = require('dotenv')
dotenv.config()

const config = {
  server: process.env.MSSQL_HOST,
  authentication: {
    type: 'default',
    options: {
      userName: 'sa',
      password: 'yourStrong(!)Password',
    },
  },
}

const connection = new Connection(config)

new Promise((resolve, reject) => {
  connection.on('connect', function(err) {
    connection.execSql(
      new Request('CREATE DATABASE Sales', err => {
        if (err) return reject(err)
        resolve()
      })
    )
  })
})
  .then(() => {
    console.log('Successfully created database')
    console.log(false )
    console.log("===11111111====")
    console.log("===22222====")
    process.exit(false)
    return true

  })
  .catch(console.error)
